﻿using System;
using System.Net.Sockets;
namespace TCPServerProject
{
    public class Client
    {
        public TcpClient socket;
        public NetworkStream myStream;

        public int connectionID;
        private byte[] recBuffer;

        public ByteBuffer buffer;

        public Client(TcpClient _socket, int _connectionID)
        {
            if (_socket == null)
                return;

            socket = _socket;
            connectionID = _connectionID;

            socket.SendBufferSize = Constants.MAX_BUFFERSIZE;
            socket.ReceiveBufferSize = Constants.MAX_BUFFERSIZE;
            myStream = socket.GetStream();
            recBuffer = new byte[Constants.MAX_BUFFERSIZE];

            myStream.BeginRead(recBuffer, 0, socket.ReceiveBufferSize, ReceiveBufferCallback, null);
            Text.WriteLine("Incoming connection form {0}", TextType.INFO, socket.Client.RemoteEndPoint.ToString());

        }

        private void ReceiveBufferCallback(IAsyncResult ar)
        {
            try
            {
                int readBytes = myStream.EndRead(ar);

                if (readBytes <= 0)
                {
                    CloseConnection();
                    return;
                }

                byte[] newBytes = new byte[readBytes];
                Buffer.BlockCopy(recBuffer, 0, newBytes, 0, readBytes);
                ServerHandleData.HandleData(connectionID, newBytes);
                myStream.BeginRead(recBuffer, 0, socket.ReceiveBufferSize, ReceiveBufferCallback, null);
            }
            catch
            {
                CloseConnection();
                return;
            }
        }

        private void CloseConnection()
        {
            Text.WriteLine("Connection from {0} has been terminated.", TextType.INFO, socket.Client.RemoteEndPoint.ToString());
            ServerTCP.SendPlayerDisconnect(connectionID);
            Types.tempPlayer[connectionID].isPlaying = false;
            socket.Close();
            socket = null;
        }
    }
}
