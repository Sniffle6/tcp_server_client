﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCPServerProject
{
    public static class General
    {
        public static int GetTickCount()
        {
            return Environment.TickCount;
        }

        public static void InitServer()
        {
            Text.WriteLine("Loading server...", TextType.DEBUG);
            int start = GetTickCount();

            InitClients();
            ServerHandleData.InitPackets();
            ServerTCP.InitServer();

            int end = GetTickCount();

            Text.WriteLine("Server has been loaded in '{0}' ms.", TextType.DEBUG, end - start);

            //start loop here in a new thread
        }
        private static void InitClients()
        {
            for (int i = 0; i < Constants.MAX_PLAYERS; i++)
            {
                ServerTCP.clients[i] = new Client(null,0);
                Types.player[i] = new Types.PlayerRect();
                Types.tempPlayer[i] = new Types.TempPlayerRec();
            }
        }

        public static void JoinMap(int connectionID)
        {
            Types.tempPlayer[connectionID].isPlaying = true;

            ServerTCP.SendInGame(connectionID);
            ServerTCP.SendPlayerData(connectionID);
            ServerTCP.SendMapData(connectionID, HandleJson.mapData);
        }

    }
}
