﻿using System;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;

namespace TCPServerProject
{
    class Program :MonoBehaviour
    {
        private void Start()
        {

            UnityThread.initUnityThread();
            UnityData.instance.navMeshInstance = NavMesh.AddNavMeshData(UnityData.instance.navMesh);
            HandleJson.LoadObjectJson();
            HandleJson.LoadMapDataJson();
            General.InitServer();
        }

        //static Thread mainThread = new Thread(MainThread);
        //static void Main(string[] args)
        //{
        //    HandleJson.LoadObjectJson();
        //    HandleJson.LoadMapDataJson();
        //    //HandleJson.PrintItems();

        //    mainThread.Name = "MainThread";
        //    Text.WriteLine("Initializing '{0}' thread.", TextType.DEBUG, mainThread.Name);
        //    mainThread.Start();
        //}

        //static void MainThread()
        //{
        //    General.InitServer();


        //    while (true)
        //    {

        //    }
        //}
    }
}
