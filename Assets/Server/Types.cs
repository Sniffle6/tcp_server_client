﻿using System;
using System.Collections;
using System.Reflection;
using UnityEngine;
using UnityEngine.AI;

namespace TCPServerProject
{
    public class Types
    {
        public static PlayerRect[] player = new PlayerRect[Constants.MAX_PLAYERS];

        public static TempPlayerRec[] tempPlayer = new TempPlayerRec[Constants.MAX_PLAYERS];

        public struct PlayerRect
        {
            public bool hasLoggedIn;
            public int health;

            public Vector3 position;
            public Vector3 collider;
            public Quaternion rotation;
            public NavMeshPath path;
            public bool isWalking;
            public int currentPoint;
            public IEnumerator emulatePath;
            public IEnumerator EmulatePath(int connectionID, Vector3[] corners, float speed)
            {
                Types.player[connectionID].currentPoint = 0;
                float lerpTime = 1f;
                float currentLerpTime = 0f;

                //Text.WriteLine("Start position is {0}", TextType.DEBUG, Types.player[connectionID].position);
                //for (int i = 0; i < corners.Length; i++)
                //{
                //    Text.WriteLine("corner[{0}] position is {1}",TextType.DEBUG, i, corners[i]);
                //}
                Types.player[connectionID].isWalking = true;
                while (Types.player[connectionID].currentPoint < corners.Length - 1)
                {

                    float perc = currentLerpTime / lerpTime;


                    Types.player[connectionID].position = Vector3.MoveTowards(Types.player[connectionID].position, corners[Types.player[connectionID].currentPoint + 1], Time.deltaTime * 3.5f); //Vector3.Lerp(corners[currentPoint], corners[currentPoint+1], perc);


                    if (Vector3.Distance(Types.player[connectionID].position, corners[Types.player[connectionID].currentPoint + 1]) <= 0.2f)
                    {
                       // Text.WriteLine("moving player[{1}] to next point {0}", TextType.DEBUG, currentPoint + 1, connectionID);
                        Types.player[connectionID].currentPoint++;
                    }

                    //Debug.LogFormat("Player[{1}] position is {0}", Types.player[connectionID].position, connectionID);
                    yield return new WaitForEndOfFrame();

                    //currentLerpTime += 0.03575f;
                    //if (currentLerpTime > lerpTime)
                    //{
                    //    currentLerpTime = lerpTime;
                    //}

                }
                Types.player[connectionID].isWalking = false;
            }
        }

        public struct TempPlayerRec
        {
            public bool isPlaying;
        }


    }
}
