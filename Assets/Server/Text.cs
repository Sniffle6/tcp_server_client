﻿using System;
using System.Text;
using UnityEngine;
namespace TCPServerProject
{
    public static class Text
    {
        
        private static string TimeStamp()
        {
            return "[" + DateTime.Now.ToString("hh:mm:ss") + "]";
        }

        public static void WriteLine(string _msg, TextType _textType, params object[] arg)
        {
            string type = "[" + Enum.GetName(typeof(TextType), _textType) + "]";

            switch (_textType)
            {
                case TextType.DEBUG:
                    Debug.LogFormat("<color=blue>" + type + _msg + "</color>", arg);
                    break;
                case TextType.INFO:
                    Debug.LogFormat("<color=green>" + type + _msg + "</color>", arg);
                    break;
                case TextType.ERROR:
                    Debug.LogFormat("<color=red>" + type + _msg + "</color>", arg);
                    break;
                case TextType.WARNING:
                    Debug.LogFormat("<color=yellow>" + type + _msg + "</color>", arg);
                    break;
                default:
                    Debug.LogFormat("<color=black>" + type + _msg + "</color>", arg);
                    break;
            }
        }
    }
    public enum TextType
    {
        DEBUG,
        INFO,
        ERROR,
        WARNING
    }
}
