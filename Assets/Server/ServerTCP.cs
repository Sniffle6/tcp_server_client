﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace TCPServerProject
{
    public class ServerTCP
    {
        private static TcpListener serverSocket = new TcpListener(IPAddress.Any, 5555);
        public static Client[] clients = new Client[Constants.MAX_PLAYERS];

        public static void InitServer()
        {

            Text.WriteLine("Initializing server socket...", TextType.DEBUG);
            serverSocket.Start();
            serverSocket.BeginAcceptTcpClient(new AsyncCallback(ClientConnectCallback), null);
        }

        private static void ClientConnectCallback(IAsyncResult ar)
        {
            TcpClient tempClient = serverSocket.EndAcceptTcpClient(ar);
            serverSocket.BeginAcceptTcpClient(new AsyncCallback(ClientConnectCallback), null);

            for (int i = 0; i < Constants.MAX_PLAYERS; i++)
            {
                if (clients[i].socket == null)
                {
                    clients[i] = new Client(tempClient, i);
                    General.JoinMap(i);
                    return;
                }
            }
        }

        public static void SendDataTo(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((data.GetUpperBound(0) - data.GetLowerBound(0)) + 1);
            buffer.WriteBytes(data);
            clients[connectionID].myStream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null);
            buffer.Dispose();
        }
        public static void SendDataToAll(byte[] data)
        {
            for (int i = 0; i < Constants.MAX_PLAYERS; i++)
            {
                if (clients[i].socket != null)
                {
                    if (Types.tempPlayer[i].isPlaying)
                        SendDataTo(i, data);
                }
            }
        }
        public static void SendDataToAllBut(int connectionID, byte[] data)
        {
            for (int i = 0; i < Constants.MAX_PLAYERS; i++)
            {
                if (connectionID != i)
                {
                    if (clients[i].socket != null)
                    {
                        if (Types.tempPlayer[i].isPlaying)
                            SendDataTo(i, data);
                    }
                }
            }
        }


        public static byte[] PlayerData(int connectionID)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SPlayerData);
            buffer.WriteInteger(connectionID);
            buffer.WriteVector3(Types.player[connectionID].position);
            buffer.WriteShort(Types.player[connectionID].isWalking ? (short)1 : (short)0);

            if (Types.player[connectionID].isWalking)
            {
                buffer.WriteInteger(Types.player[connectionID].path.corners.Length - Types.player[connectionID].currentPoint);
                for (int i = Types.player[connectionID].currentPoint; i < Types.player[connectionID].path.corners.Length; i++)
                {
                    buffer.WriteVector3(Types.player[connectionID].path.corners[i]);
                }
            }

            return buffer.ToArray();
        }


        public static void SendInGame(int connectionID)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SInGame);
            buffer.WriteInteger(connectionID);
            SendDataTo(connectionID, buffer.ToArray());
            buffer.Dispose();
        }
        public static void SendPlayerData(int connectionID)
        {

            for (int i = 0; i < Constants.MAX_PLAYERS; i++)
            {
                if (clients[i] != null)
                {
                    if (Types.tempPlayer[i].isPlaying)
                    {
                        if (connectionID != i)
                        {
                            SendDataTo(connectionID, PlayerData(i));
                        }
                    }
                }
            }

            SendDataToAll(PlayerData(connectionID));
        }
        public static void SendPlayerDisconnect(int connectionID)
        {

            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SDisconnect);
            buffer.WriteInteger(connectionID);
            SendDataToAllBut(connectionID, buffer.ToArray());
        }
        public static void SendPlayerMove(int connectionID, Vector3 pos, Quaternion rot)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SPlayerMove);
            buffer.WriteInteger(connectionID);
            buffer.WriteVector3(pos);
            buffer.WriteQuaternion(rot);


            SendDataToAllBut(connectionID, buffer.ToArray());
        }
        public static void SendPlayerMoveToAll(int connectionID, Vector3 pos, Quaternion rot)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SPlayerMove);
            buffer.WriteInteger(connectionID);
            buffer.WriteVector3(pos);
            buffer.WriteQuaternion(rot);


            SendDataToAll(buffer.ToArray());
        }
        public static void SendPlayerAnimation(int connectionID, string name, int value)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SPlayerAnimation);
            buffer.WriteInteger(connectionID);

            buffer.WriteString(name);
            buffer.WriteInteger(value);

            SendDataToAll(buffer.ToArray());
        }
        public static void SendWorldObjectData(int connectionID, int index, string name, string description)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SPlayerWorldObjectLeftClick);
            buffer.WriteInteger(connectionID);

            buffer.WriteInteger(index);
            buffer.WriteString(name);
            buffer.WriteString(description);

            SendDataTo(connectionID, buffer.ToArray());
        }
        public static void SendPlayerWalk(int connectionID, Vector3 position)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SSendPlayerWalk);
            buffer.WriteInteger(connectionID);

            buffer.WriteVector3(position);

            SendDataToAll(buffer.ToArray());
        }

        public static void SendMapData(int connectionID, List<MapObjectData> data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SSendMapData);
            buffer.WriteInteger(connectionID);

            buffer.WriteInteger(data.Count);

            for (int i = 0; i < data.Count; i++)
            {
                buffer.WriteInteger(data[i].enumerationIndex);
                buffer.WriteInteger(data[i].modelIndex);
                buffer.WriteVector3(data[i].position);
                buffer.WriteVector3(data[i].eulerAngles);
            }

            SendDataTo(connectionID, buffer.ToArray());
        }
        public static void SendPlayerPath(int connectionID, Vector3[] path)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInteger((int)ServerPackets.SSendPlayerPath);
            buffer.WriteInteger(connectionID);

            buffer.WriteInteger(path.Length);

            for (int i = 0; i < path.Length; i++)
            {
                buffer.WriteVector3(path[i]);
            }

            SendDataToAll(buffer.ToArray());
        }
    }
}
