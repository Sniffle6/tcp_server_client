﻿using UnityEngine;
namespace TCPServerProject
{
    class Constants
    {
        public const int MAX_PLAYERS = 5000;
        //Networking
        public const int MAX_BUFFERSIZE = 4096;

        public static Vector3 SPAWN_POSITION = new Vector3(2.51f, 1.46f, -2.05f);
    }
}
