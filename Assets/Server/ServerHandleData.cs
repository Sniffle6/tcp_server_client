﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;


namespace TCPServerProject
{
    public class ServerHandleData
    {
        public delegate void Packet_(int connectionID, byte[] data);
        public static Dictionary<int, Packet_> packets = new Dictionary<int, Packet_>();
        private static int pLength;
        public static void InitPackets()
        {
            Text.WriteLine("Initializing network messages...", TextType.DEBUG);
            packets.Add((int)ClientPackets.CMovement, HandleMovement);
            packets.Add((int)ClientPackets.CAnimation, HandleAnimations);
            packets.Add((int)ClientPackets.CWorldObjLeftClick, HandleWorldObjectLeftClick);
            packets.Add((int)ClientPackets.CPathDestination, HandleSetPlayerDestination);

        }

        public static void HandleData(int connectionID, byte[] data)
        {
            byte[] buffer = (byte[])data.Clone();

            if (ServerTCP.clients[connectionID].buffer == null)
            {
                ServerTCP.clients[connectionID].buffer = new ByteBuffer();
            }
            ServerTCP.clients[connectionID].buffer.WriteBytes(buffer);
            if (ServerTCP.clients[connectionID].buffer.Count() == 0)
            {
                ServerTCP.clients[connectionID].buffer.Clear();
                return;
            }
            if (ServerTCP.clients[connectionID].buffer.Length() >= 4)
            {
                pLength = ServerTCP.clients[connectionID].buffer.ReadInteger(false);
                if (pLength <= 0)
                {
                    ServerTCP.clients[connectionID].buffer.Clear();
                    return;
                }
            }
            while (pLength > 0 & pLength <= ServerTCP.clients[connectionID].buffer.Length() - 4)
            {
                if (pLength <= ServerTCP.clients[connectionID].buffer.Length() - 4)
                {
                    ServerTCP.clients[connectionID].buffer.ReadInteger();
                    data = ServerTCP.clients[connectionID].buffer.ReadBytes(pLength);

                    HandleDataPackets(connectionID, data);
                }

                pLength = 0;
                if (ServerTCP.clients[connectionID].buffer.Length() >= 4)
                {
                    pLength = ServerTCP.clients[connectionID].buffer.ReadInteger(false);
                    if (pLength < 0)
                    {
                        ServerTCP.clients[connectionID].buffer.Clear();
                        return;
                    }
                }

                if (pLength <= 1)
                {
                    ServerTCP.clients[connectionID].buffer.Clear();
                }
            }
        }
        private static void HandleDataPackets(int connectionID, byte[] data)
        {
            int packetID;
            ByteBuffer buffer;
            buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            packetID = buffer.ReadInteger();
            buffer.Dispose();
            Packet_ packet;
            if (packets.TryGetValue(packetID, out packet))
            {
                Text.WriteLine("[cID]" + connectionID + " [Size]" + buffer.Length() + " [Packet]" + Enum.GetName(typeof(ClientPackets), packetID), TextType.DEBUG);
                packet.Invoke(connectionID, data);
            }
        }

        private static void HandleMovement(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            Vector3 pos = buffer.ReadVector3();
            Quaternion rot = buffer.ReadQuaternion();
            Types.player[connectionID].position = pos;
            Types.player[connectionID].rotation = rot;

            ServerTCP.SendPlayerMove(connectionID, pos, rot);

        }
        private static void HandleSetPlayerDestination(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            Vector3 pos = buffer.ReadVector3();

            Text.WriteLine("Received packet and executing code", TextType.DEBUG);
            UnityThread.executeInUpdate(() =>
            {
                UnityData.instance.CalculatePath(connectionID, pos);
            });



            //ServerTCP.SendPlayerWalk(connectionID, pos);

        }

        private static void HandleAnimations(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            string name = buffer.ReadString();
            int value = buffer.ReadInteger();

            ServerTCP.SendPlayerAnimation(connectionID, name, value);

        }
        private static void HandleWorldObjectLeftClick(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteBytes(data);
            buffer.ReadInteger();

            int index = buffer.ReadInteger();
            Vector3 pos = buffer.ReadVector3();

            ItemData item = HandleJson.Items[index];
            Text.WriteLine("Index = {0}, Name = {1},  Description = {2}", TextType.DEBUG, item.itemID, item.itemName, item.itemDescription);
            Text.WriteLine("Distance from object is " + Vector3.Distance(pos, Types.player[connectionID].position), TextType.DEBUG);
            Text.WriteLine("position = " + pos, TextType.DEBUG);
            var angle = pos - Types.player[connectionID].position;
            var angle2 = angle = angle.normalized;
            var finalPos= (pos + (0.5f * angle));
            Text.WriteLine("position after math = " + finalPos, TextType.DEBUG);
            if (Vector3.Distance(pos, Types.player[connectionID].position) > 1.5f)
            {
                ServerTCP.SendPlayerWalk(connectionID, finalPos);
            }
            else
            {
                ServerTCP.SendWorldObjectData(connectionID, item.itemID, item.itemName, item.itemDescription);
            }

        }
    }
}
