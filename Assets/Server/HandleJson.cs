﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

//[Serializable]
//public class MapObjectData
//{
//    public int enumerationIndex;
//    public int modelIndex;
//    public Vector3 position;
//    public Vector3 eulerAngles;
//}
//public class ItemData
//{
//    public int itemID { get; set; }
//    public string itemName { get; set; }
//    public string itemDescription { get; set; }
//}
public static class HandleJson
{
    


    public static Dictionary<int, ItemData> Items { get; } = new Dictionary<int, ItemData>();

    public static List<MapObjectData> mapData = new List<MapObjectData>();
    static MapObjectData m_Data;
    static JArray jsonArray;

    public static void LoadObjectJson()//"Assets/Resources/MapChunk.txt"
    {
        using (StreamReader r = new StreamReader("Assets/Resources/ObjectData.json"))
        {
            string json = r.ReadToEnd();
            foreach (var item in JsonConvert.DeserializeObject<List<ItemData>>(json))
            {
                Items.Add(item.itemID, item);
            }

        }

    }
    public static void LoadMapDataJson()
    {
        string line;
        using (StreamReader sw = File.OpenText("Assets/Resources/MapChunk.txt"))
        {
            line = sw.ReadToEnd();
        }

        jsonArray = JArray.Parse(line);

        foreach (JObject o in jsonArray.Children<JObject>())
        {
            m_Data = new MapObjectData
            {
                enumerationIndex = (int)o.SelectToken("enumerationIndex"),
                modelIndex = (int)o.SelectToken("modelIndex")
            };
            m_Data.position.x = (float)o.SelectToken("position.x");
            m_Data.position.y = (float)o.SelectToken("position.y");
            m_Data.position.z = (float)o.SelectToken("position.z");

            m_Data.eulerAngles.x = (float)o.SelectToken("eulerAngles.x");
            m_Data.eulerAngles.y = (float)o.SelectToken("eulerAngles.y");
            m_Data.eulerAngles.z = (float)o.SelectToken("eulerAngles.z");

            mapData.Add(m_Data);
        }

    }
    public static void PrintItems()
    {
        foreach (var item in Items)
        {
            Console.WriteLine("Index ={0}, Name ={1},  Description ={2}", item.Value.itemID, item.Value.itemName, item.Value.itemDescription);
        }
    }
}




