﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace TCPServerProject
{
    public class UnityData : MonoBehaviour
    {
        #region instance
        public static UnityData instance;
        private void Awake()
        {
            instance = this;
        }
        #endregion

        public NavMeshDataInstance navMeshInstance = new NavMeshDataInstance();
        public NavMeshData navMesh;
        public NavMeshPath path;
        private void Start()
        {
            path = new NavMeshPath();
        }

        public void CalculatePath(int connectionID, Vector3 pos)
        {
            NavMeshQueryFilter filter = new NavMeshQueryFilter
            {
                agentTypeID = 0,
                areaMask = NavMesh.AllAreas
            };
            Types.player[connectionID].path = new NavMeshPath();
            //Text.WriteLine("position {0}, position to go to {1}", TextType.DEBUG, Types.player[connectionID].position, pos);
            NavMesh.CalculatePath(Types.player[connectionID].position, pos, filter, Types.player[connectionID].path);

            ServerTCP.SendPlayerPath(connectionID, Types.player[connectionID].path.corners);



            if (Types.player[connectionID].emulatePath != null)
                StopCoroutine(Types.player[connectionID].emulatePath);

            Types.player[connectionID].emulatePath = Types.player[connectionID].EmulatePath(connectionID, Types.player[connectionID].path.corners, 3.5f);
            StartCoroutine(Types.player[connectionID].emulatePath);
            // return UnityData.instance.path.corners;
            //for (int i = 0; i < UnityData.instance.path.corners.Length; i++)
            //{
            //    Text.WriteLine(UnityData.instance.path.corners[i].ToString(), TextType.DEBUG);
            //}

        }


        public static float GetPathLength(Vector3[] corners)
        {
            float lng = 0.0f;

            if (corners.Length > 1)
            {
                for (int i = 1; i < corners.Length; ++i)
                {
                    lng += Vector3.Distance(corners[i - 1], corners[i]);
                }
            }

            return lng;
        }
    }
}

