﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data Object", menuName = "Objects/Data")]
public class DataObject : ScriptableObject
{
    public ObjectType _type;
    public int _index;
}
