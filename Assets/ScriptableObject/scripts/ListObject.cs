﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Object List", menuName = "Objects/List")]
public class ListObject : ScriptableObject {

    public GameObject[] _types;
}
