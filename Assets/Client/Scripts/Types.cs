﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;

public class Types
{
    public static PlayerRect[] players = new PlayerRect[Constants.MAX_PLAYERS];



    public struct PlayerRect
    {
        public GameObject playerPref;

        public int connectionID;

        public int health;

        public Vector3 position;
        public Vector3 collider;
        public Quaternion rotation;
        public Vector3[] cornersOfPath;
        public bool isWalking;
    }
}

