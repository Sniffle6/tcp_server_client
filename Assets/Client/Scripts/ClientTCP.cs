﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class ClientTCP
{
    private static TcpClient clientSocket;
    private static NetworkStream myStream;
    private static byte[] asyncBuffer;

    public static void InitClient(string _address, int _port)
    {
        clientSocket = new TcpClient
        {
            ReceiveBufferSize = Constants.MAX_BUFFERSIZE,
            SendBufferSize = Constants.MAX_BUFFERSIZE
        };
        asyncBuffer = new byte[Constants.MAX_BUFFERSIZE * 2];
        clientSocket.BeginConnect(_address, _port, new AsyncCallback(ClientConnectCallback), clientSocket);
    }

    private static void ClientConnectCallback(IAsyncResult ar)
    {
        clientSocket.EndConnect(ar);
        if (clientSocket.Connected == false)
            return;
        else
        {
            clientSocket.NoDelay = true;
            myStream = clientSocket.GetStream();
            myStream.BeginRead(asyncBuffer, 0, Constants.MAX_BUFFERSIZE * 2, ReceiveCallback, null);
            Debug.Log("Succesfully connected to server!");
        }
    }

    private static void ReceiveCallback(IAsyncResult ar)
    {
        try
        {
            int readBytes = myStream.EndRead(ar);
            if (readBytes <= 0)
                return;

            byte[] newBytes = new byte[readBytes];
            Buffer.BlockCopy(asyncBuffer, 0, newBytes, 0, readBytes);

            UnityThread.executeInUpdate(() =>
            {
                ClientHandleData.HandleData(newBytes);
            });
            myStream.BeginRead(asyncBuffer, 0, Constants.MAX_BUFFERSIZE * 2, ReceiveCallback, null);

        }
        catch
        {

            return;
        }
    }

    public static void DisconnectFromServer()
    {
        clientSocket.Close();
        clientSocket = null;
    }
    public static void SendData(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteInteger((data.GetUpperBound(0) - data.GetLowerBound(0)) + 1);
        buffer.WriteBytes(data);
        myStream.Write(buffer.ToArray(), 0, buffer.ToArray().Length);
        buffer.Dispose();
    }

    public static void SendMovement(Vector3 position, Quaternion rotation)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteInteger((int)ClientPackets.CMovement);
        buffer.WriteVector3(position);
        buffer.WriteQuaternion(rotation);
        SendData(buffer.ToArray());
    }
    public static void SendPathDestination(Vector3 position)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteInteger((int)ClientPackets.CPathDestination);
        buffer.WriteVector3(position);
        SendData(buffer.ToArray());
    }
    public static void SendAnimation(string name, int value)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteInteger((int)ClientPackets.CAnimation);

        buffer.WriteString(name);
        buffer.WriteInteger(value);

        SendData(buffer.ToArray());
    }
    public static void SendWorldObjectLeftClick(Vector3 pos, int index)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteInteger((int)ClientPackets.CWorldObjLeftClick);

        buffer.WriteInteger(index);
        buffer.WriteVector3(pos);

        SendData(buffer.ToArray());
    }
    //public static void RequestPath(int connectionID, )
    //{

    //}
}

