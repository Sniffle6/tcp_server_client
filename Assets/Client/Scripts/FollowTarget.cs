﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform _target;
    public Vector3 _offset;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!_target)
            if (Types.players[ClientManager.instance.myConnectionID].playerPref != null)
                _target = Types.players[ClientManager.instance.myConnectionID].playerPref.transform;
        if (_target)
            transform.position = _target.position + _offset;
    }
}
