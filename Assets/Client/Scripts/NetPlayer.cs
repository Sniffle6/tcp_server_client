﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NetPlayer : MonoBehaviour
{
    const string RUN_ANIMATION = "Run";

    public int myConnectionID;


    NavMeshAgent m_Agent;
    RaycastHit m_HitInfo = new RaycastHit();
    Animator m_Anim;
    public bool isWalking;
    int currentNavPoint = 0;
    Vector3[] navCorners;
    // Use this for initialization
    void Start()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_Anim = GetComponent<Animator>();

    }
    // Update is called once per frame
    void Update()
    {
        if (!m_Agent.pathPending && isWalking && m_Anim.GetBool(RUN_ANIMATION) == true)
        {
            if (m_Agent.remainingDistance < 0.1f)
            {
                GotoNextPoint();
            }
        }

        if (myConnectionID != ClientManager.instance.myConnectionID)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out m_HitInfo))
            {
                //if (m_HitInfo.collider.gameObject.GetComponent<Data>())
                // {
                //     ClickedOnWorldObject(m_HitInfo.transform.position, m_HitInfo.collider.gameObject.GetComponent<Data>());
                // }
                // else
                // {
                Walk(m_HitInfo.point);
                //  }
            }
        }


    }
    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (navCorners.Length == 0)
            return;

        if (currentNavPoint >= navCorners.Length)
        {
            transform.position = navCorners[navCorners.Length-1];
            isWalking = false;
            m_Anim.SetBool(RUN_ANIMATION, false);
            return;
        }

        // Set the agent to go to the currently selected destination.
        m_Agent.SetDestination(navCorners[currentNavPoint]);

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        // currentNavPoint = (currentNavPoint + 1) % navCorners.Length;
        currentNavPoint += 1;
    }
    public void Walk(Vector3 position)
    {
        // Move(); 
        NetworkAnimator.SetBool(m_Anim, RUN_ANIMATION, true);

        ClientTCP.SendPathDestination(position);
    }
    public void SetPath(int connectionID, Vector3[] pathCorners, int cPoint = 0)
    {
        if (m_Anim)
            m_Anim.SetBool(RUN_ANIMATION, true);
        isWalking = true;
        navCorners = pathCorners;
        currentNavPoint = cPoint;
        GotoNextPoint();
    }
    public void RecieveWalk(Vector3 position)
    {
        m_Anim.SetBool(RUN_ANIMATION, true);
        isWalking = true;
        m_Agent.SetDestination(position);
    }
    public void ClickedOnWorldObject(Vector3 pos, Data obj)
    {
        switch (obj.data._type)
        {
            case ObjectType.Object:
                //Move();
                ClientTCP.SendWorldObjectLeftClick(pos, obj.data._index);
                break;
            case ObjectType.NPC:
                break;
            case ObjectType.PLAYER:

                break;
            default:
                break;
        }
    }
    //public void Move()
    //{
    //    ClientTCP.SendMovement(transform.position, transform.rotation);

    //}
}
