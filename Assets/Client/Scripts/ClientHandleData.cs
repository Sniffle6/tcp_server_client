﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
public class ItemData
{
    public int itemID { get; set; }
    public string itemName { get; set; }
    public string itemDescription { get; set; }
}
public class ClientHandleData
{
    private static ByteBuffer playerBuffer;

    public delegate void Packet_(byte[] data);
    public static Dictionary<int, Packet_> packets = new Dictionary<int, Packet_>();
    private static int pLength;

    public static void InitPackets()
    {
        packets.Add((int)ServerPackets.SInGame, HandleInGame);
        packets.Add((int)ServerPackets.SPlayerData, HandlePlayerData);
        packets.Add((int)ServerPackets.SPlayerMove, HandlePlayerMove);
        packets.Add((int)ServerPackets.SDisconnect, HandlePlayerDisconnect);
        packets.Add((int)ServerPackets.SPlayerAnimation, HandlePlayerAnimations);
        packets.Add((int)ServerPackets.SPlayerWorldObjectLeftClick, HandlePlayerWorldObjectLeftClick);
        packets.Add((int)ServerPackets.SSendPlayerWalk, HandleWalkPlayerToPosition);
        packets.Add((int)ServerPackets.SSendMapData, HandleMapData);
        packets.Add((int)ServerPackets.SSendPlayerPath, HandleSetPlayerPath);
    }
    public static void HandleData(byte[] data)
    {
        byte[] buffer = (byte[])data.Clone();

        if (playerBuffer == null)
        {
            playerBuffer = new ByteBuffer();
        }
        playerBuffer.WriteBytes(buffer);
        if (playerBuffer.Count() == 0)
        {
            playerBuffer.Clear();
            return;
        }
        if (playerBuffer.Length() >= 4)
        {
            pLength = playerBuffer.ReadInteger(false);
            if (pLength <= 0)
            {
                playerBuffer.Clear();
                return;
            }
        }
        while (pLength > 0 & pLength <= playerBuffer.Length() - 4)
        {
            if (pLength <= playerBuffer.Length() - 4)
            {
                playerBuffer.ReadInteger();
                data = playerBuffer.ReadBytes(pLength);

                HandleDataPackets(data);
            }

            pLength = 0;
            if (playerBuffer.Length() >= 4)
            {
                pLength = playerBuffer.ReadInteger(false);
                if (pLength < 0)
                {
                    playerBuffer.Clear();
                    return;
                }
            }

            if (pLength <= 1)
            {
                playerBuffer.Clear();
            }
        }
    }
    private static void HandleDataPackets(byte[] data)
    {
        int packetID; Packet_ packet;
        ByteBuffer buffer;
        buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetID = buffer.ReadInteger();
        buffer.Dispose();
        if (packets.TryGetValue(packetID, out packet))
        {
            Debug.Log("<Packet>" + Enum.GetName(typeof(ServerPackets), packetID));
            packet.Invoke(data);
        }
    }

    private static void HandleInGame(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();


        ClientManager.instance.myConnectionID = buffer.ReadInteger();

        buffer.Dispose();
    }

    private static void HandlePlayerData(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();
        Types.players[connectionID].position = buffer.ReadVector3();
        short value = buffer.ReadShort();

        Types.players[connectionID].connectionID = connectionID;

        if (value == 1)
            Types.players[connectionID].isWalking = true;
        else if (value == 0)
            Types.players[connectionID].isWalking = false;

        if (value == 1)
        {
            int length = buffer.ReadInteger();
            Types.players[connectionID].cornersOfPath = new Vector3[length];
            Debug.LogFormat("Length is {0}", Types.players[connectionID].cornersOfPath.Length);
            for (int i = 0; i < length; i++)
            {
                Types.players[connectionID].cornersOfPath[i] = buffer.ReadVector3();
            }
        }
        buffer.Dispose();

        ClientManager.instance.InstantiateNetworkPlayer(connectionID);
        //if (Types.players[ClientManager.instance.myConnectionID].playerPref)
        //     Types.players[ClientManager.instance.myConnectionID].playerPref.GetComponent<NetPlayer>().Move();
    }

    public static void HandlePlayerMove(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();
        Types.players[connectionID].position = buffer.ReadVector3();
        Types.players[connectionID].rotation = buffer.ReadQuaternion();
        Types.players[connectionID].playerPref.GetComponent<NavMeshAgent>().Warp(Types.players[connectionID].position);
        Types.players[connectionID].playerPref.transform.rotation = Types.players[connectionID].rotation;

        buffer.Dispose();
    }
    public static void HandlePlayerDisconnect(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();


        buffer.Dispose();

        ClientManager.instance.DestroyNetworkPlayer(connectionID);
    }
    public static void HandlePlayerAnimations(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();

        string name = buffer.ReadString();
        int value = buffer.ReadInteger();


        buffer.Dispose();

        if (value == 0)
        {
            Types.players[connectionID].playerPref.GetComponent<Animator>().SetBool(name, false);
        }
        else if (value == 1)
        {
            Types.players[connectionID].playerPref.GetComponent<Animator>().SetBool(name, true);
        }

    }
    public static void HandlePlayerWorldObjectLeftClick(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();

        int itemID = buffer.ReadInteger();
        string name = buffer.ReadString();
        string description = buffer.ReadString();

        ItemData item = new ItemData
        {
            itemID = itemID,
            itemName = name,
            itemDescription = description
        };

        Debug.Log(item.itemID + ", " + item.itemName + ", " + item.itemDescription);

        buffer.Dispose();


    }
    public static void HandleWalkPlayerToPosition(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();
        Types.players[connectionID].playerPref.GetComponent<NetPlayer>().RecieveWalk(buffer.ReadVector3());

        buffer.Dispose();


    }
    static MapObjectData d;
    public static void HandleMapData(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();
        int count = buffer.ReadInteger();

        for (int i = 0; i < count; i++)
        {
            d = new MapObjectData
            {
                enumerationIndex = buffer.ReadInteger(),
                modelIndex = buffer.ReadInteger(),
                position = buffer.ReadVector3(),
                eulerAngles = buffer.ReadVector3()
            };
            ClientManager.instance.MAP_DATA.Add(d);
        }
        GenerateMap.instance.InstantiateMap();

        buffer.Dispose();
    }
    public static void HandleSetPlayerPath(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadInteger();

        int connectionID = buffer.ReadInteger();
        int count = buffer.ReadInteger();
        Vector3[] corners = new Vector3[count];
        for (int i = 0; i < count; i++)
        {
            corners[i] = buffer.ReadVector3();
        }
        Types.players[connectionID].playerPref.GetComponent<NetPlayer>().SetPath(connectionID, corners);

        buffer.Dispose();
    }
}

