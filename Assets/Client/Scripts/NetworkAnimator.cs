﻿using UnityEngine;

public static class NetworkAnimator
{
    public static bool GetBool(Animator animator, string name)
    {
        //get the state from the server
        //this isnt finished
        return animator.GetBool(name);
    }
    public static void SetBool(Animator animator, string name, bool value)
    {
        ClientTCP.SendAnimation(name, value ? 1 : 0);
    }
}

