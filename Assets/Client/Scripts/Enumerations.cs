﻿public enum ServerPackets
{
    SInGame = 1,
    SPlayerData,
    SDisconnect,
    SPlayerMove,
    SPlayerAnimation,
    SPlayerWorldObjectLeftClick,
    SSendPlayerWalk,
    SSendMapData,
    SSendPlayerPath

}

public enum ClientPackets
{
    CNewAccount,
    CLogin,
    CMovement,
    CAnimation,
    CWorldObjLeftClick,
    CPathDestination,
    CRequestPath
}

public enum ObjectType
{
    Ground = 1,
    Floor,
    Object,
    NPC,
    PLAYER
}
