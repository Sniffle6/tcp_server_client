﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClientManager : MonoBehaviour
{
    public static ClientManager instance;

    public int myConnectionID;
    public List<MapObjectData> MAP_DATA = new List<MapObjectData>();

    [SerializeField]
    private GameObject connectionPrefab;
    [SerializeField]
    private string ipAddress;
    [SerializeField]
    private int port;
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
        UnityThread.initUnityThread();

        InitPlayer();

        ClientHandleData.InitPackets();
        ClientTCP.InitClient(ipAddress, port);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void InitPlayer()
    {
        for (int i = 0; i < Constants.MAX_PLAYERS; i++)
        {
            Types.players[i] = new Types.PlayerRect();
        }
    }

    public void InstantiateNetworkPlayer(int connectionID)
    {
        Types.players[connectionID].playerPref = Instantiate(connectionPrefab);
        Types.players[connectionID].playerPref.name = "Player: " + connectionID;
        Types.players[connectionID].playerPref.GetComponent<NetPlayer>().myConnectionID = connectionID;
        if (Types.players[connectionID].isWalking)
        {
            Types.players[connectionID].playerPref.GetComponent<NavMeshAgent>().Warp(Types.players[connectionID].cornersOfPath[0]);
            StartCoroutine(setPathNextFrame(connectionID));
        }
        else
        {
            Types.players[connectionID].playerPref.GetComponent<NavMeshAgent>().Warp(Types.players[connectionID].position);
        }
        //if (connectionID != myConnectionID) 
        //{
        //    Types.players[connectionID].playerPref.GetComponentInChildren<Camera>().enabled = false;
        //    Types.players[connectionID].playerPref.GetComponentInChildren<AudioListener>().enabled = false;
        //}

    }
    IEnumerator setPathNextFrame(int connectionID)
    {
        yield return new WaitForSeconds(0.2f);
        Types.players[connectionID].playerPref.GetComponent<NetPlayer>().SetPath(connectionID, Types.players[connectionID].cornersOfPath, 2);
    }
    public void DestroyNetworkPlayer(int connectionID)
    {
        Destroy(Types.players[connectionID].playerPref);
    }

    private void OnApplicationQuit()
    {
        ClientTCP.DisconnectFromServer();
    }
}
