﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

    class ByteBuffer
    {

        private List<byte> Buff;
        private byte[] readBuff;
        private int readPos;
        private bool buffUpdated = false;

        public ByteBuffer()
        {
            Buff = new List<byte>();
            readPos = 0;
        }

        #region Functions
        public int GetReadPos()
        {
            return readPos;
        }
        public byte[] ToArray()
        {
            return Buff.ToArray();
        }
        public int Count()
        {
            return Buff.Count;
        }
        public int Length()
        {
            return Count() - readPos;
        }
        public void Clear()
        {
            Buff.Clear();
            readPos = 0;
        }
        #endregion


        #region WriteData
        public void WriteBytes(byte[] input)
        {
            Buff.AddRange(input);
            buffUpdated = true;
        }
        public void WriteShort(short input)
        {
            Buff.AddRange(BitConverter.GetBytes(input));
            buffUpdated = true;
        }
        public void WriteInteger(int input)
        {
            Buff.AddRange(BitConverter.GetBytes(input));
            buffUpdated = true;
        }
        public void WriteLong(long input)
        {
            Buff.AddRange(BitConverter.GetBytes(input));
            buffUpdated = true;
        }
        public void WriteFloat(float input)
        {
            Buff.AddRange(BitConverter.GetBytes(input));
            buffUpdated = true;
        }
        public void WriteString(string input)
        {
            Buff.AddRange(BitConverter.GetBytes(input.Length));
            Buff.AddRange(Encoding.ASCII.GetBytes(input));
            buffUpdated = true;
        }
        public void WriteVector3(Vector3 input)
        {
            byte[] vectorArray = new byte[sizeof(float) * 3];

            Buffer.BlockCopy(BitConverter.GetBytes(input.x), 0, vectorArray, 0 * sizeof(float), sizeof(float));
            Buffer.BlockCopy(BitConverter.GetBytes(input.y), 0, vectorArray, 1 * sizeof(float), sizeof(float));
            Buffer.BlockCopy(BitConverter.GetBytes(input.z), 0, vectorArray, 2 * sizeof(float), sizeof(float));

            Buff.AddRange(vectorArray);
            buffUpdated = true;
        }
        public void WriteQuaternion(Quaternion input)
        {
            byte[] vectorArray = new byte[sizeof(float) * 4];

            Buffer.BlockCopy(BitConverter.GetBytes(input.x), 0, vectorArray, 0 * sizeof(float), sizeof(float));
            Buffer.BlockCopy(BitConverter.GetBytes(input.y), 0, vectorArray, 1 * sizeof(float), sizeof(float));
            Buffer.BlockCopy(BitConverter.GetBytes(input.z), 0, vectorArray, 2 * sizeof(float), sizeof(float));
            Buffer.BlockCopy(BitConverter.GetBytes(input.w), 0, vectorArray, 3 * sizeof(float), sizeof(float));

            Buff.AddRange(vectorArray);
            buffUpdated = true;
        }
        #endregion

        #region ReadData
        public byte[] ReadBytes(int Length, bool peek = true)
        {
            if (Buff.Count > readPos)
            {
                if (buffUpdated)
                {
                    readBuff = Buff.ToArray();
                    buffUpdated = false;
                }

                byte[] value = Buff.GetRange(readPos, Length).ToArray();
                if (peek)
                    readPos += Length;
                return value;
            }
            else
            {
                throw new Exception("[Byte[]]You either read out incorrect values, or the 'ByteBuffer' is empty");
            }
        }
        public short ReadShort(bool Peek = true)
        {
            if (Buff.Count > readPos)
            {
                if (buffUpdated)
                {
                    readBuff = Buff.ToArray();
                    buffUpdated = false;
                }

                short value = BitConverter.ToInt16(readBuff, readPos);
                if (Peek & Buff.Count > readPos)
                    readPos += 2;
                return value;
            }
            else
            {
                throw new Exception("[SHORT]You either read out incorrect values, or the 'ByteBuffer' is empty");
            }
        }
        public int ReadInteger(bool Peek = true)
        {
            if (Buff.Count > readPos)
            {
                if (buffUpdated)
                {
                    readBuff = Buff.ToArray();
                    buffUpdated = false;
                }

                int value = BitConverter.ToInt32(readBuff, readPos);
                if (Peek & Buff.Count > readPos)
                    readPos += 4;
                return value;
            }
            else
            {
                throw new Exception("[INT32]You either read out incorrect values, or the 'ByteBuffer' is empty");
            }
        }
        public long ReadLong(bool Peek = true)
        {
            if (Buff.Count > readPos)
            {
                if (buffUpdated)
                {
                    readBuff = Buff.ToArray();
                    buffUpdated = false;
                }

                long value = BitConverter.ToInt64(readBuff, readPos);
                if (Peek & Buff.Count > readPos)
                    readPos += 8;
                return value;
            }
            else
            {
                throw new Exception("[LONG]You either read out incorrect values, or the 'ByteBuffer' is empty");
            }
        }
        public float ReadFloat(bool Peek = true)
        {
            if (Buff.Count > readPos)
            {
                if (buffUpdated)
                {
                    readBuff = Buff.ToArray();
                    buffUpdated = false;
                }

                float value = BitConverter.ToInt64(readBuff, readPos);
                if (Peek & Buff.Count > readPos)
                    readPos += 4;
                return value;
            }
            else
            {
                throw new Exception("[FLOAT]You either read out incorrect values, or the 'ByteBuffer' is empty");
            }
        }
        public string ReadString(bool Peek = true)
        {
            int length = ReadInteger(true);
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            string value = Encoding.ASCII.GetString(readBuff, readPos, length);

            if (Peek & Buff.Count > readPos)
            {
                if (value.Length > 0)
                    readPos += length;
            }
            return value;
        }
        public Vector3 ReadVector3(bool Peek = true)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }
            byte[] value = Buff.GetRange(readPos, sizeof(float) * 3).ToArray();
            Vector3 vector3;
            vector3.x = BitConverter.ToSingle(value, 0 * sizeof(float));
            vector3.y = BitConverter.ToSingle(value, 1 * sizeof(float));
            vector3.z = BitConverter.ToSingle(value, 2 * sizeof(float));

            if (Peek)
            {
                readPos += sizeof(float) * 3;
            }

            return vector3;
        }
        public Quaternion ReadQuaternion(bool Peek = true)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }
            byte[] value = Buff.GetRange(readPos, sizeof(float) * 4).ToArray();
            Quaternion quaternion;
            quaternion.x = BitConverter.ToSingle(value, 0 * sizeof(float));
            quaternion.y = BitConverter.ToSingle(value, 1 * sizeof(float));
            quaternion.z = BitConverter.ToSingle(value, 2 * sizeof(float));
            quaternion.w = BitConverter.ToSingle(value, 3 * sizeof(float));

            if (Peek)
            {
                readPos += sizeof(float) * 4;
            }

            return quaternion;
        }


        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                Buff.Clear();
                readPos = 0;
            }
            disposedValue = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

