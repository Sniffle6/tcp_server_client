﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
[Serializable]
public class MapObjectData
{
    public int enumerationIndex;
    public int modelIndex;
    public Vector3 position;
    public Vector3 eulerAngles;
}
[Serializable]
public class NavMeshInfo
{
    public int[] areas;
    public int[] indices;
    public Vector3[] vertices;
}
public class SaveMap : MonoBehaviour
{
    public List<MapObjectData> list = new List<MapObjectData>();
    DataObject dObj;
    Transform obj;
    MapObjectData data;
    NavMeshInfo navInfo;
    // Use this for initialization
    void Start()
    {
        GenerateJson();
       // GenerateNavMeshJson();

        //use same method as map data but store the two int's and the vector3 inside triangle., takinga  break now (Y)
    }
    void GenerateNavMeshJson()
    {
        NavMeshTriangulation triangle = NavMesh.CalculateTriangulation();

        navInfo = new NavMeshInfo();

        Array.Resize<int>(ref navInfo.areas, triangle.areas.Length);
        for (int i = 0; i < triangle.areas.Length; i++)
        {
            navInfo.areas[i] = triangle.areas[i];
        }
        Array.Resize<int>(ref navInfo.indices, triangle.indices.Length);
        for (int i = 0; i < triangle.indices.Length; i++)
        {
            navInfo.indices[i] = triangle.indices[i];
        }
        Array.Resize<Vector3>(ref navInfo.vertices, triangle.vertices.Length);
        for (int i = 0; i < triangle.vertices.Length; i++)
        {
            navInfo.vertices[i] = triangle.vertices[i];
        }
         
        WriteFile("Assets/Resources/NavChunk.txt", JsonConvert.SerializeObject(navInfo, Formatting.Indented));

    }
    void GenerateJson()
    {
        int childCount = transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            obj = transform.GetChild(i);
            if (!obj.GetComponent<Data>())
                return;
            dObj = obj.GetComponent<Data>().data;
            switch (dObj._type)
            {
                case ObjectType.Ground:
                    data = new MapObjectData
                    {
                        enumerationIndex = 1,
                        modelIndex = dObj._index,
                        position = obj.position,
                        eulerAngles = obj.eulerAngles
                    };
                    list.Add(data);
                    break;
                case ObjectType.Floor:
                    data = new MapObjectData
                    {
                        enumerationIndex = 2,
                        modelIndex = dObj._index,
                        position = obj.position,
                        eulerAngles = obj.eulerAngles
                    };
                    list.Add(data);
                    break;
                case ObjectType.Object:
                    data = new MapObjectData
                    {
                        enumerationIndex = 3,
                        modelIndex = dObj._index,
                        position = obj.position,
                        eulerAngles = obj.eulerAngles
                    };
                    list.Add(data);
                    break;
                default:
                    break;
            }
        }
        WriteFile("Assets/Resources/MapChunk.txt", JsonConvert.SerializeObject(list, Formatting.Indented));
    }
    void WriteFile(string path,string jsonString)
    {
        using (StreamWriter sw = File.AppendText(path))
        {
            sw.WriteLine(jsonString);
        }
    }
}