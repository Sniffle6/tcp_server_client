﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AI;

public class GenerateMap : MonoBehaviour
{
    public NavMeshData navMeshAsset;
    NavMeshDataInstance navMeshInstance = new NavMeshDataInstance();
    NavMeshPath path;
    public static GenerateMap instance;
    public ListObject[] listObjects;
    public List<MapObjectData> mapData = new List<MapObjectData>();
    MapObjectData m_Data;
    JArray jsonArray;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        path = new NavMeshPath();
      //  LoadJson();

        //
    }

    void LoadJson()
    {

        string line;
        using (StreamReader sw = File.OpenText("Assets/Resources/MapChunk.txt"))
        {
            line = sw.ReadToEnd();
        }

        jsonArray = JArray.Parse(line);

        foreach (JObject o in jsonArray.Children<JObject>())
        {
            m_Data = new MapObjectData
            {
                enumerationIndex = (int)o.SelectToken("enumerationIndex"),
                modelIndex = (int)o.SelectToken("modelIndex")
            };
            m_Data.position.x = (float)o.SelectToken("position.x");
            m_Data.position.y = (float)o.SelectToken("position.y");
            m_Data.position.z = (float)o.SelectToken("position.z");

            m_Data.eulerAngles.x = (float)o.SelectToken("eulerAngles.x");
            m_Data.eulerAngles.y = (float)o.SelectToken("eulerAngles.y");
            m_Data.eulerAngles.z = (float)o.SelectToken("eulerAngles.z");

            mapData.Add(m_Data);
        }
    }
    GameObject tempObj;
   public void InstantiateMap()
    {
        if (ClientManager.instance.MAP_DATA.Count <= 0)
        {
            Debug.LogWarning("No map data!");
            return;
        }
        int count = ClientManager.instance.MAP_DATA.Count;
        for (int i = 0; i < count; i++)
        {
            switch (ClientManager.instance.MAP_DATA[i].enumerationIndex)
            {
                case 1:
                    tempObj = Instantiate(listObjects[0]._types[ClientManager.instance.MAP_DATA[i].modelIndex], transform);
                    tempObj.transform.position = ClientManager.instance.MAP_DATA[i].position;
                    tempObj.transform.eulerAngles = ClientManager.instance.MAP_DATA[i].eulerAngles;
                    break;
                case 2:
                    tempObj = Instantiate(listObjects[1]._types[ClientManager.instance.MAP_DATA[i].modelIndex], transform);
                    tempObj.transform.position = ClientManager.instance.MAP_DATA[i].position;
                    tempObj.transform.eulerAngles = ClientManager.instance.MAP_DATA[i].eulerAngles;
                    break;
                case 3:
                    tempObj = Instantiate(listObjects[2]._types[ClientManager.instance.MAP_DATA[i].modelIndex], transform);
                    tempObj.transform.position = ClientManager.instance.MAP_DATA[i].position;
                    tempObj.transform.eulerAngles = ClientManager.instance.MAP_DATA[i].eulerAngles;
                    break;
                default:
                    break;
            }
        }
        //GetComponent<NavMeshSurface>().UpdateNavMesh(navMeshAsset);
        navMeshInstance = NavMesh.AddNavMeshData(navMeshAsset);
        //GetComponent<NavMeshSurface>().BuildNavMesh();
    }
}
